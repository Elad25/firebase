
<?php
require 'src/firebaseLib.php';
const DEFAULT_URL = 'https://angular-24a28.firebaseio.com/';
const DEFAULT_TOKEN = '';
const DEFAULT_PATH = '/users';

$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);

if (isset($_POST['id'])) { // Check if the user send the form
	$id = $_POST['id'];
	$name = $_POST['name'];
	$email = $_POST['email'];
	$radio = $_POST['radio'];// The radio button selection -> 1/2/3
	if ($name == '' && $email == '' && $radio == '1') // Check if the other fields are empty  and the user choose 'create', if yes -> delete user
	{
		//delete this user with delete
	$firebase->delete(DEFAULT_PATH.'/'.$id);
	}
		
	else if ($id == '' && $radio == '1'){	// If the id field empty and the user choose 'create' -> create new user
			//create newUser with "push"
	$newUser = array(
			"name" => $name,
			"email" => $email,
	);
	$newUserId = $firebase->push(DEFAULT_PATH.'/'.$id, $newUser);
	}
	
	else if ($radio == '1'){ 			// If the other fields are full and the user choose 'create'-> insert the new user
	//create newUser with "push"
	$newUser = array(
			"name" => $name,
			"email" => $email,
	);
	$newUserId = $firebase->push(DEFAULT_PATH.'/'.$id, $newUser);
	}

	else if ($radio == '2'){ // The user choose 'update' 
	
		///////update existing user1 with "update"
		$newUser = array(
			"name" => $name,
			"email" => $email,
			);
		$firebase->update(DEFAULT_PATH.'/'.$id, $newUser);
	}
	
	else if ($radio == '3'){ // The user choose 'overwrite' 
	
		///////replace existing user with "set"
		$newUser = array(
			"name" => $name,
			"email" => $email,
			);
		$firebase->set(DEFAULT_PATH.'/'.$id, $newUser);
	}
}

echo $newUserId;

 ?>